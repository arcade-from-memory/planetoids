extends Node

export (PackedScene) var Bullet
export (PackedScene) var Planetoid
export var level = 0
var screen_size = 600

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	screen_size = get_viewport().get_visible_rect().size
	new_level()

func _on_Ship_shoot():
	var bullet = Bullet.instance()
	bullet.rotation = $Ship.rotation
	bullet.position = $Ship.position + 40 * Vector2.UP.rotated(bullet.rotation)
	add_child(bullet)
	
func new_level():
	var num_planetoids = (randi() % 6) + 1
	for i in range(0, num_planetoids):
		spawn_planetoid()
		
func spawn_planetoid():
	var planetoid = Planetoid.instance()
	planetoid.position.x = randi() % int(screen_size.x)
	planetoid.position.y = randi() % int(screen_size.y)
	while (planetoid.position - $Ship.position).length() < 100:
		planetoid.position.x = randi() % int(screen_size.x)
		planetoid.position.y = randi() % int(screen_size.y)
	add_child(planetoid)
