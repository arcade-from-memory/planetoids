extends Area2D

var tumble # how fast it spins (in radians)
var trajectory # angle that it moves towards
var speed # how fast it goes
var screen_size

func _ready():
	screen_size = get_viewport_rect().size
	tumble = rand_range(-PI, PI)
	trajectory = rand_range(0, 2*PI)
	speed = rand_range(100, 500)

func _process(delta):
	rotation += tumble * delta
	position += delta * speed * Vector2.UP.rotated(trajectory)
	position.x = wrapf(position.x, 0.0, screen_size.x)
	position.y = wrapf(position.y, 0.0, screen_size.y)
