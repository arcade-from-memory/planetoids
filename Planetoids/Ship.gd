extends Area2D


# Declare member variables here. Examples:
export var thrust = 10
export var max_speed = 400
export var angular_speed = PI / 12 # in radians/sec
var screen_size
var velocity
signal shoot

# Called when the node enters the scene tree for the first time.
func _ready():
	screen_size = get_viewport_rect().size
	velocity = Vector2()
	position.x = 400
	position.y = 300
	
func _process(delta):
	if Input.is_action_pressed("ui_up"):
		velocity += thrust * Vector2.UP.rotated(rotation)
		velocity = velocity.clamped(max_speed)
		$AnimatedSprite.animation = "thrusting"
		$AnimatedSprite.play()
	else:
		$AnimatedSprite.animation = "not_thrusting"
		$AnimatedSprite.stop()
	if Input.is_action_pressed("ui_left"):
		rotation -= angular_speed * delta
	elif Input.is_action_pressed("ui_right"):
		rotation += angular_speed * delta
	if Input.is_action_just_pressed("ui_accept"):
		emit_signal("shoot")
	position += velocity * delta
	position.x = wrapf(position.x, 0.0, screen_size.x)
	position.y = wrapf(position.y, 0.0, screen_size.y)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
