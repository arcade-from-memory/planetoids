extends Area2D

export var speed = 1000
export var lifetime = 590 # lifetime in pixels
var velocity
var screen_size
var distance
var life = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	screen_size = get_viewport_rect().size
	velocity = speed * Vector2.UP.rotated(rotation)
	
func _process(delta):
	position += velocity * delta
	position.x = wrapf(position.x, 0.0, screen_size.x)
	position.y = wrapf(position.y, 0.0, screen_size.y)
	life += speed * delta
	if life > lifetime:
		queue_free()
